// 生成安全取值工具类代码
import 'syntax.dart';

class GenerateSafeAccessCode extends ClassDefinition{
  static const String safeAccessTemplate = '''
/// Safe access to a property of an object.
///
/// If the value does not exist or is null then it will return `defaultValue`.
///
/// Example usage:
/// ```dart
///   print(safeAccess(map,'someKey', defaultValue:'myDefaultValue'));
/// ```
class SafeAccess {
 T safeAccess<T>(Map map, String key, {required T defaultValue}) {
   var val = map[key];
   if (val!= null) {
     try {
       return val as T;
     } catch (_) {}
   }
   return defaultValue;
 }

 static int safeParseInt(Object? src, {int def = 0}) {
    if (src == null) {
      return def;
    } else if (src is int) {
      return src;
    } else {
      return int.tryParse(safeParseString(src)) ?? def;
    }
  }

  static double safeParseDouble(Object? src, {double def = 0.0}) {
    if (src == null) {
      return def;
    } else if (src is double) {
      return src;
    } else {
      return double.tryParse(safeParseString(src)) ?? def;
    }
  }

  static String safeParseString(Object? src, {String def = ""}) {
    if (src == null) {
      return def;
    } else {
      if (src is String) {
        return src;
      } else {
        return src.toString();
      }
    }
  }

  static bool safeParseBoolean(Object? src, {bool def = false}) {
    if (src == null) {
      return def;
    } else {
      if (src is bool) {
        return src;
      }else if (src is int){
        return (src == 1);
      } else {
        if (src.toString() == 'true' || src.toString() == 'false'){
          return (src.toString() == 'true');
        }
        return (src.toString() == '1');
      }
    }
  }

  static List safeParseList(Object? src, {List def = const []}) {
    if (src != null && src is List) {
      return src;
    } else {
      return def;
    }
  }

  static Map safeParseMap(Object? src, {Map def = const {}}) {
    if (src != null && src is Map) {
      return src;
    } else {
      return def;
    }
  }

  static bool isListEmpty(List? list) {
    return (list == null || list.isEmpty);
  }
}
''';

  GenerateSafeAccessCode(String name) : super(name);

  @override
  toString() => safeAccessTemplate;
}